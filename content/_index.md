+++
+++

# Startpages

A startpage is the first thing you should see when you open your browser or
create a new tab. Most modern browsers spin up their own "homepage", Firefox
presents you with the top sites you visit, a quick web search, and potentially
some news updates.

Creating your own startpage has several advantages (as well as disadvantages)
over using conventional homepages.

## Why you should make your own

Startpages are great, they are completely yours and you control every aspect of them. From the
colours of the links, to the pictures you want, and given a little more time and effort even things
like custom news trackers/weather widgets/mini-games!

### Pros

- Entirely customise _the most visited page_ on your browser
- Learn the basics of web development
- Unleash your creativity
- Learn more about design

### Cons

- Time consuming
- Browser updates can cause your startpage to break

It's a great hobby to pick up, and who knows you might be like me and end up pivoting your entire
career because of them!

{% banner() %}
We have a guide to help you create a basic startpage available
[here](/guides/basic-startpage).
{% end %}

We also have other [guides](/guides) available covering various topics.

## FAQ

Most people directed here will likely benefit from reading these first, they
answer the most commonly asked questions:

### How do I set my startpage to act as a new tab page?

We have guides for both [Chrome](/guides/chrome-startpage) and
[Firefox](/guides/firefox-startpage).

These two methods should remain unchanged for a while, but if your startpage
randomly stops working at some point it may be due to an update. Most browsers
tend to fight against these overrides.

Let us know if anything goes wrong.

### Why are my styles not working in a way I'd expect them to?

Every browser ships it's own "default" stylesheet, these are the
_bare minimum_ styles required to make a page feel like an HTML website (not
just plain-text).

You should make heavy use of the **inspect element** feature on your browser,
this can usually be accessed by simply right clicking anywhere, and attempt to
identify the styles that might be doing you wrong.

The most common styles people have trouble with:

#### There is a weird gap all around my page

Most browsers add a small 4-10px margin around the body of every website.
You can remove it using:

```css
body {
  margin: 0;
}
```

#### My padding/margins/widths feel _off_

Browsers render widths and heights of elements using specific methods, some of
them can be weird. You can use the snippet below to unify the sizing a bit and
make it behave in a more consistent way.

```css
* {
  box-sizing: border-box;
}
```

You can find a more in-depth explanation of this on the
[css-tricks website](https://css-tricks.com/box-sizing/)

If you want you, you can also have a look at the default user agent styles that
[Firefox](https://github.com/mozilla/gecko-dev/blob/master/layout/style/res/html.css)
and [Chrome](https://chromium.googlesource.com/chromium/blink/+/master/Source/core/css/html.css)
ship with.

### How do I horizontally/vertically align an element?

tl;dr: [Read about it here](https://developer.mozilla.org/en-US/docs/Web/CSS/Layout_cookbook/Center_an_element)
