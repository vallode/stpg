+++
title = "Useful links"
+++

# Useful links

Before we put together a decent source of truth for “good” startpages, here is a collection of links
where you can find startpages made by other users:

<https://startpages.github.io/>  
<https://www.reddit.com/r/startpages/>
