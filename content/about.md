+++
title = "About"
+++

[stpg.tk](/) serves as a source-of-truth for all things /stpg/ (startpage).
Here you will find guides from the community, FAQs, etc.

The site is built as publicly as possible and we open source whatever we can (including data).
It uses a very simple stack:

- Static site generation: [Zola](https://www.getzola.org/)
- Hosting: [Netlify](https://netlify.com/)
- Analytics: [GoatCounter](https://stpg.goatcounter.com/)
- Git Repository: [Gitlab](https://gitlab.com/vallode/stpg)

If you think there is information missing from this page please let us know.
