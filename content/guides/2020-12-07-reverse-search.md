+++
title = "Reverse searching images"
description = "Learn how to find the original, full-resolution source as well as artists of wallpapers."
date = 2020-12-07
+++

You found a cool piece of art, but it's low resolution! It has all kinds
of compression artefacts! We'll be covering various techniques to find
the original source of the image, hopefully in higher quality.

We'll be starting with the most obvious ways to find an image's source,
and move onto more sophisticated methods. This is what you typically want
to be doing to not waste any time.

## Filenames

Your first instinct might tell you do "just reverse search it" but in a lot of cases the images
you might find shared online have all the information you need already in them.

Was the image uploaded to a service that preserves filenames? Filenames often
contain information that point to the source.

Sometimes, it contains the artist's name and/or the piece's title, which you can then easily google.

- ArtStation:

```
sheng-lam-1-phasebook-present.jpg
```

`sheng-lam` is the name of the artist, `phasebook-present` is the name of the art piece. If
searching the keywords together doesn't reveal anything, try to search for the artist themselves.
You might even be able to ask them about if it you find contact details!

- DeviantArt:

```
wallpaper_by_el_felipe_d1f378y.jpg
```

`wallpaper` is the name of the image, `el_felipe` is the artist/uploader.

- Danbooru:

```
__usami_renko_and_maribel_hearn_touhou_drawn_by_re_ghotion__0c76c8a4bd9605ec5966ad3d58622c3d.png
```

`usami_renko_and_maribel_hearn_touhou` is a list of the tags used and `re_ghotion` is the artist.

- Screenshots:

```
[MTBB] Spice and Wolf - 10 [94B01165].mkv_000122.165.webp
```

This one can vary a lot, different kinds of playback software has all kinds of different formats for
it's screenshot names. The key is just to _look_ and try to extract any information. This one can
tell you pretty much everything about the specific origin of the picture.

`[MTBB]` is the release group, `Spice and Wolf` is the show, `10` is the episode number and what
follows is more metadata.

- Manga:

```
Sousou_no_Frieren_-_Ch.21_-_Coward_-_19.jpg
```

Similarly to the above. `Sousou_no_Frieren` is the name of the manga, `Ch.21` is the chapter,
`Coward` is the name of the chapter, and `19` is the page!

Some sites have less obvious but still recognizable patterns.

- `73418995_p0.png`: this image comes from [Pixiv](https://www.pixiv.net/en/), you can access the post with its ID at [/artworks/73418995](https://www.pixiv.net/en/artworks/73418995)
- `sample_fe49839ca513985368b57f45990c5023.jpeg`: this filename contains an md5 hash from Gelbooru, and can be found by searching [md5:fe49839ca513985368b57f45990c5023](https://gelbooru.com/index.php?page=post&s=list&tags=md5%3afe49839ca513985368b57f45990c5023). Most booru websites support this, so try this query on the common ones, it's not necessarily from Gelbooru.
- `EctzmNOUEAABzUj.jpg`: this image comes from Twitter, it's not possible to get the original tweet from this alone but the image can be accessed at [pbs.twimg.com](https://pbs.twimg.com/media/EctzmNOUEAABzUj?format=jpg&name=orig). Later techniques cover how to find the original tweet.

{% banner() %}
Be aware that while the service you found the picture on may preserve filenames, it may also alter them.
{% end %}

Discord, for example, removes special characters and replaces whitespace with underscores.

This `[MTBB] Spice and Wolf - 10 [94B01165].mkv_000122.165.webp` turns into `MTBB_Spice_and_Wolf_-_10_94B01165.mkv_000122.165.webp`.

## Text

Does your image contain text? Any text at all?

It could be the artist's twitter handle in the corner.

Google, within quotes, any phrases contained in the image.

If your image contains foreign text you can't type out yourself, run it through [Google Cloud Vision](https://cloud.google.com/vision/docs/drag-and-drop) and click the "Text" tab, or [Yandex OCR](https://translate.yandex.com/ocr) and press "Open in Yandex.Translate"

## Pre-processing your image

The techniques above have failed, and you now want to reverse search your image.
You first have to make sure your image has the least obstructions possible.

Does it have black bars, some gallery app's UI in the way? Crop it out.

Does your image look flipped? Fix it.

## Anime

Does it look like anime?

- [SauceNAO](https://saucenao.com/): for anime-style art as well as anime screenshots
- [iqdb](https://iqdb.org/): exclusively for anime-style art
- [trace.moe](https://trace.moe/): exclusively for anime screenshots

Does your image have a non-standard resolution?

If it looks like an anime screenshot but is taller or wider than standard
aspect ratios, try cropping it to 16:9 (or 4:3 for older-looking productions)

## Yandex

[Yandex.Images](https://yandex.com/images/) is very underrated for image reverse searching.
In the author's experience, it often gives better results than Google Images.

If the picture you're searching is a little risqué, don't forget to click
the shield icon in the top-right corner and select "Unsecure" search.
This won't filter out NSFW results.

## Google

You're getting desperate, maybe [Google Images](https://images.google.com/) can help.

## Extensions

Going to all of these websites and uploading your picture can take a while,
thankfully people have made easy to use extensions to reverse search
pictures in a jiffy.

We can't recommend a single one, most of them do the exact same thing, so it's better to just link a
few we think are good:

- [Search by image for Chrome store](https://chrome.google.com/webstore/detail/search-by-image/cnojnbdhbhnkbcieeekonklommdnndci?hl=en)
- [Search by image for Firefox](https://addons.mozilla.org/en-US/firefox/addon/search_by_image/)
- [Reverse Image Search for Firefox](https://addons.mozilla.org/en-US/firefox/addon/image-reverse-search/)

For most users the above is fine, if you are into user scripts you might find this one helpful:

- [Search by image user script](https://greasyfork.org/en/scripts/2998-search-by-image)

## Practice

As with most things, practice makes ~~perfect~~ better, so try to download the image below and find
its source!

![Cartoon girl with glasses standing](/images/reverse-search-demo.jpg)

_Note: will probably be harder than most images you find, but if you can do this one you're doing
great._

## Out of luck?

As a last resort, try flipping the image and reverse search it again.

If none of these worked for you, try asking friends familiar with the art style.  
If it's somebody's profile picture, try asking them about it.

If the above image has you stumped, we know the source! Just ask us on
[discord](https://discord.gg/wmJXckE) :)
