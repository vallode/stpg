# /wg/ - /stpg/

A general website built for the startpage community, join us on [discord](https://discord.gg/xW8r4nX).
Hosts blog posts about startpages and topics relevant to our community.

The guides here are usually not all-purpose web development introductions, they lean towards
outlines of processes. Thus they provide links to further reading for those who might need them.

## Building

The only dependency is [zola](https://www.getzola.org/).

To run:
`zola serve`

## Creating new guides/posts

You are totally welcome to submit a [merge request](https://gitlab.com/vallode/stpg/-/merge_requests)
with a new guide. There really are no rules aside from it being acceptable quality (my own writing is not
great so do not feel pressured to be shakespear!) and for it to have a purpose on the site.

i.e if you create a guide on skateboarding, while very cool, it probably will not be included on the site.

